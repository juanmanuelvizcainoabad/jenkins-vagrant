#!/bin/bash
#create user jenkins and put the ssh key
mkdir /root/.ssh -p
apt-get update && apt-get install git -y
cp /home/vagrant/id_rsa  /root/.ssh/.
cp /home/vagrant/id_rsa.pub  /root/.ssh/.
cp /home/vagrant/authorized_keys  /root/.ssh/.
rm /home/vagrant/id_rsa
rm /home/vagrant/authorized_keys
rm /home/vagrant/id_rsa.pub
chmod 400 /root/.ssh/* -R
