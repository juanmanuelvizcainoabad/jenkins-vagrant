#!/bin/bash
#create user jenkins and put the ssh key

mkdir /root/.ssh -p
cp /home/vagrant/id_rsa  /root/.ssh/.
cp /home/vagrant/id_rsa.pub  /root/.ssh/.
cp /home/vagrant/authorized_keys  /root/.ssh/.
rm /home/vagrant/id_rsa
rm /home/vagrant/authorized_keys
rm /home/vagrant/id_rsa.pub
chmod 400 /root/.ssh/* -R
#Install lastest version from jekins repo
apt-get update && apt-get install  git apt-transport-https openjdk-7-jre curl -y
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
echo "deb http://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/sources.list
apt-get update && apt-get install jenkins -y
